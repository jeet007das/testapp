import React, { Component } from "react";
import '../stylesheet/card.css';


class Card extends Component {
    constructor(props) {
      super(props);
      this.state = {
        
      };
    }


    detailsExtraction(details){
        var str
        if(details && details.length > 100){
            str = details.substring(0,100);
            return str
        }else{
            return str
        }
    }

    render(){
        const {name, details} = this.props.data
        return(
            <div className="card">
                <div className="heading">
                    <h3>{name}</h3>
                </div>
                <div className="details">
                    <p>{this.detailsExtraction(details)}</p>
                </div>
            </div>
        )
    }
}

export default Card;
