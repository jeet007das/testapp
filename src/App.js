import logo from './logo.svg';
import './App.css';
import MainApp from "./main"

function App() {
  return (
    <div className="App">
        <MainApp />
    </div>
  );
}

export default App;
