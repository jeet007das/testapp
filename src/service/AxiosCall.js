import {baseUrl}  from "../config"
import axios from "axios";
export const getData= async() =>{
    try{
        const response = await axios.get(baseUrl);
         console.log(response);
         if(response.status === 200){
             const {data} = response
             if(data.length)return data
             else return []
         }else{
             return []
         }
    }catch(e){
        console.log(e)
        alert(e.message)
        return []
        
    }
}
