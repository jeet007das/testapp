import React, { Component } from "react";
import Card from "./components/cards"
import "./stylesheet/main.css"
import {getData} from "./service/AxiosCall"



class Main extends Component {
    constructor(props) {
      super(props);
      this.state = {
        data: []
      };
    }


    async componentDidMount(){
        const data = await getData()
        this.setState({data})
    }

    render(){

        const {data}= this.state
        return(
            <div className="main">
                {
                (this.state.data).length > 0 ?
                  ( this.state.data.map(spaceData => {
                     return(
                        <Card 
                            data={spaceData}/>
                     )
                            
                    })): (
                        <h1>No, data found</h1>
                    )
                }
                
            </div>
        )
    }
}

export default Main;
